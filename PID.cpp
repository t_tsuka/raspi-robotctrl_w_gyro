#include "PID.hpp"

PID::PID(){
	err_sum = err_old = 0;
	P = 1; 
	I = 0;
	D = 0;
	LimitMax = 1e30;
	LimitMin = -1e30;
}

PID::PID(double Ps, double Is, double Ds){
	err_sum = err_old = 0;
	LimitMax = 1e30;
	LimitMin = -1e30;
	SetParam(Ps, Is, Ds);
}

PID::~PID(){
}

int PID::SetParam(double Ps, double Is, double Ds){
	P = Ps; I = Is; D = Ds;
}

int PID::Reset(){
	err_sum = err_old = 0;
}

double PID::Exec(double err_in, double dedt, double dt){
	double ret;
	double err_sum_tmp;

	err_sum_tmp = err_sum;
	err_sum += (err_old+err_in)*0.5 * dt;

	ret = P*(err_in + I*err_sum + D*dedt);

	if(ret > LimitMax){
		ret = LimitMax;
		err_sum = err_sum_tmp;
	}
	if(ret < LimitMin){
		ret = LimitMin;
		err_sum = err_sum_tmp;
	}

	err_old = err_in;

	return ret;
}

double PID::Exec(double err_in, double dt){
	double ret;
	double dedt;
	double err_sum_tmp;

	dedt = (err_in - err_old)/dt;
	err_sum_tmp = err_sum;
	err_sum += (err_old+err_in)*0.5 * dt;

	ret = P*(err_in + I*err_sum + D*dedt);

	if(ret > LimitMax){
		ret = LimitMax;
		err_sum = err_sum_tmp;
	}
	if(ret < LimitMin){
		ret = LimitMin;
		err_sum = err_sum_tmp;
	}

	err_old = err_in;

	return ret;
}

int PID::SetLimit(double Max, double Min){
	if(Max > Min){
		LimitMax = Max;
		LimitMin = Min;
	}
}



