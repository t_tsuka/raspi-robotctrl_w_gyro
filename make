#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>   //get_timeofday(), struct timeval
#include <time.h>       //gmtime(), localtime(), struct tm 
#include "DRV8830.hpp"
#include "MPU9250.hpp"

struct timeval NextTime(struct timeval tm0, double dt){
	long sec, usec;
	struct timeval next;

	sec = (long)dt;
	usec = (long)(( dt - (double)((long)dt) ) * 1e6 );

	next.tv_sec = tm0.tv_sec + sec;
	next.tv_usec = tm0.tv_usec + usec;

	printf("sec=%ld, usec=%ld (%lf)\n", sec, usec, dt);

	return next;
}

double TimeDiff(struct timeval tm1, struct timeval tm0){
	double dt;
	dt = (double)(tm1.tv_sec - tm0.tv_sec) 
		+(double)(tm1.tv_usec - tm0.tv_usec)*1e-6;
	return dt;
}


int main(int argc, char* argv[]){
	int i, ret;
	char str[256];
	char *com, *period, *arg1, *arg2;
	double speed;
	FILE *fp;
	double dt;
	short accI[3], rotI[3], tempI;
	double acc[3], rot[3], temp;
	double SF_Acc, SF_Gyro, SF_Temp;

	DRV8830 MotR, MotL;
	MPU9250 Sensor;
	struct timeval tm, tm0, tm_next;

//Read Command file
	if(argc != 2){
		printf("Usage: %s [command file]\n",argv[0]);
		return -1;
	}
	if(NULL == (fp=fopen(argv[1], "r"))){
		printf("File Open Error: %s\n", argv[1]);
		return -1;
	}

//Init MPU9250
	ret = Sensor.CheckConnection();
	if(!ret){
		printf("MPU9250 Not Found\n");
		return -1;
	}
	Sensor.Init();
	
	Sensor.CheckConnection_Mag();
	if(!ret){
		printf("MPU9250(Magnetometer) Not Found\n");
		return -1;
	}

	Sensor.SetAccFullScale (Acc_FS_4G);
	Sensor.SetGyroFullScale( Gyro_FS_250dps);
	Sensor.SetAccRate( Acc_BW460Hz_SR1k );
	Sensor.SetGyroRate( Gyro_BW250Hz_SR8k );

	printf("MPU9250 Initialized\n");

//Init Motor Drivers
	int fdR = MotR.Init( 0x63 );
	int fdL = MotL.Init( 0x64 );

	printf("fd(R)=%d\n", fdR);
	printf("fd(L)=%d\n", fdL);

	if( (fdR<0) || (fdL<0)){
		return -1;
	}

	MotR.PrintFaultMsg( MotR.ReadFault() );
	MotL.PrintFaultMsg( MotL.ReadFault() );

	gettimeofday(&tm0, NULL);
	tm_next = NextTime(tm0, 1.4);

	while(1){
		//gettimeofday(&tm, NULL);
		Sensor.ReadData(&tm, accI, rotI, &tempI);
		dt=TimeDiff(tm, tm0);
		for(int i=0; i<3; i++){
			acc[i] = (double)accI[i] * Sensor.ScaleFactor_Acc();
			rot[i] = (double)rotI[i] * Sensor.ScaleFactor_Gyro();
		}
		temp = (double)tempI * Sensor.ScaleFactor_Temp() + Sensor.Offset_Temp();

		//printf("%lf, a=(%

		if(TimeDiff(tm, tm_next) > 0)
			break;
		
		
		tm0 = tm;
	}

	return 0;



	while( ! feof(fp) ){

//Read commnad from file
		fgets(str, 256, fp);
		printf("%s", str);
		com = strtok(str, " ");

//Comment, blank line -> skip
		if( com == NULL) continue;
		if( strlen(com) == 0) continue;
		if( (com[0] == '#') || com[0] == '\n') continue;

		period = strtok(NULL, " ");
		arg1 = strtok(NULL, " ");
		arg2 = strtok(NULL, " ");


		if( !strncmp( com, "FWD", 3 ) ){	//Forward
			speed = atof(arg1) * 5.06;	// 0 < arg1 < 1
			MotR.Run( -speed );
			MotL.Run( speed );

		}else if( !strncmp( com, "REV", 3) ){	//Reverse
			speed = atof(arg1) * 5.06;	// 0 < arg1 < 1
			MotR.Run( speed );
			MotL.Run( -speed );

		}else if( !strncmp( com, "ROT", 3) ){	//Rotate
			if( arg1[0] == 'R' ){
				speed = atof(arg2) * 5.06 ;	// 0 < arg1 < 1
				MotR.Run( speed );
				MotL.Run( speed );

			}else if( arg1[0] == 'L'){
				speed = atof(arg2) * 5.06 ;	// 0 < arg1 < 1
				MotR.Run( -speed );
				MotL.Run( -speed );
			}

		}else if( !strncmp(com, "STOP", 4) ){
			MotR.Stop();
			MotL.Stop();
			
		}else if( !strncmp(com, "BRK", 3) ){
			MotR.Brake();
			MotL.Brake();

		}else if( !strncmp(com, "END", 3) ){
			MotR.Stop();
			MotL.Stop();
			break;
			
		}else{
			MotR.Stop();
			MotL.Stop();
		}

		usleep( atof(period)*1e6 );

	}//end of while()

	MotR.Stop();
	MotL.Stop();

	fclose(fp);

}
