#ifndef __PID_H__
#define __PID_H__

class PID{
public:
	PID();
	PID(double Pset, double Iset, double Dset);
	~PID();

	int SetParam(double Pset, double Iset, double Dset);
	int Reset();
	double Exec(double err_in, double dt);
	double Exec(double err_in, double de_dt, double dt);
	int SetLimit(double Max, double Min);


private:
	double P, I, D;
	double err_sum, err_old;
	double LimitMax, LimitMin;

};

#endif
