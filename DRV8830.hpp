#ifndef __ADS1015_H__
#define __ADS1015_H__


#define DRV8830_CONFIG 0x00
#define DRV8830_FAULT  0x01


#define DRV8830_CONFIG_STANDBY 0x00
#define DRV8830_CONFIG_FWD 0x01
#define DRV8830_CONFIG_REV 0x02
#define DRV8830_CONFIG_BRAKE 0x03

typedef enum{
	FWD = 0,
	REV
}motor_dir;

class DRV8830{
private:
	int fd;
	int reg_config;
	int i2c_addr;

public:
	DRV8830();
	~DRV8830();
	int GetFD();
	int Init( int I2Caddr );
	int Stop();
	int Brake();
	int Run(motor_dir dir, int i_vset);
	int Run( double vset);
	int ClearFault();
	int ReadFault();
	int PrintFaultMsg( int msg );

};
#endif
