/*
	motor cotrol (DRV8830)

	2018.6.25 T.Tsukamoto
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include "DRV8830.hpp"
#include <math.h>

#define DEV_PATH "/dev/i2c-1" 



DRV8830::DRV8830(){
	reg_config = 0x00;
	i2c_addr = 0;
	fd = 0;
}

DRV8830::~DRV8830(){
	if(fd > 0){
		close(fd);
	}
}

int DRV8830::GetFD(){
	return fd;
}

int DRV8830::Init(int I2Caddr){
	i2c_addr = I2Caddr;
	fd = open(DEV_PATH, O_RDWR);	
	if(fd < 0){
		fprintf(stderr, "I2C Open Error\n");
		return -1;
	}

	//set I2C Addr
	if( ioctl(fd, I2C_SLAVE, I2Caddr) < 0){
		fprintf(stderr, "I2C address set failed\n");
		return -2;
	}

	this->Stop();

	return fd;
}


int DRV8830::Stop( ){
	reg_config = 0x00;
	return  i2c_smbus_write_byte_data(fd, DRV8830_CONFIG, reg_config);
}


int DRV8830::Brake( ){
	reg_config = DRV8830_CONFIG_BRAKE;
	return  i2c_smbus_write_byte_data(fd, DRV8830_CONFIG, reg_config);
}


int DRV8830::Run( motor_dir dir, int i_vset){
	
	if(dir == FWD)
		reg_config = DRV8830_CONFIG_FWD;
	else
		reg_config = DRV8830_CONFIG_REV;

	reg_config |= (i_vset << 2);

	return  i2c_smbus_write_byte_data(fd, DRV8830_CONFIG, reg_config);
}



int DRV8830::Run( double vset){
	const double coe = 12.4505929;
	int i_vset = vset * coe;
	return Run( vset>0? FWD: REV, fabs(i_vset) );
}

int DRV8830::ClearFault(){
	return  i2c_smbus_write_byte_data(fd, DRV8830_FAULT, (1<<7));
}

int DRV8830::ReadFault(){
	int reg;
	reg = i2c_smbus_read_byte_data(fd, DRV8830_FAULT);
	return reg;
}

int DRV8830::PrintFaultMsg( int msg){
	int ret;
	//printf("msg=%02x\n",msg);
	if( ! (msg & (1)) ){
		printf("DRV8830(addr=%x), No Fault \n", i2c_addr);
		ret = 0;
	}else{
		if( msg & (1<<1) )
			printf("DEV8830(addr=%x), Over Current Protection\n", i2c_addr);
		if( msg & (1<<2) )
			printf("DEV8830(addr=%x), Low Voltage\n", i2c_addr);
		if( msg & (1<<3) )
			printf("DEV8830(addr=%x), Over Heat\n", i2c_addr);
		if( msg & (1<<4) )
			printf("DEV8830(addr=%x), Current Limit\n", i2c_addr);
		ret = 1;
	}	
	return ret;
}




