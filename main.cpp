#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>   //get_timeofday(), struct timeval
#include <time.h>       //gmtime(), localtime(), struct tm 
#include <math.h>
#include "DRV8830.hpp"
#include "MPU9250.hpp"
#include "PID.hpp"

struct timeval NextTime(struct timeval tm0, double dt){
	long sec, usec;
	struct timeval next;

	sec = (long)dt;
	usec = (long)(( dt - (double)((long)dt) ) * 1e6 );

	next.tv_sec = tm0.tv_sec + sec;
	next.tv_usec = tm0.tv_usec + usec;

	printf("sec=%ld, usec=%ld (%lf)\n", sec, usec, dt);

	return next;
}

double TimeDiff(struct timeval tm1, struct timeval tm0){
	double dt;
	dt = (double)(tm1.tv_sec - tm0.tv_sec) 
		+(double)(tm1.tv_usec - tm0.tv_usec)*1e-6;
	return dt;
}


int ReadNextCommand(FILE *fp, double *SetSpeed, double *SetRot, double *Period, int *run){
	char str[256], *com, *arg1, *arg2, *arg3;
	int ret = 0;

	while( !feof(fp) ){
//Read commnad from file
		fgets(str, 256, fp);
		printf("%s", str);
		com = strtok(str, " ");

//Comment, blank line -> skip
		if( com == NULL) continue;
		if( strlen(com) == 0) continue;
		if( (com[0] == '#') || com[0] == '\n') continue;

		arg1 = strtok(NULL, " ");
		arg2 = strtok(NULL, " ");
		arg3 = strtok(NULL, " ");


// RUN [period]  [speed]  [rotation]
		if( !strncmp( com, "RUN", 3 ) ){	
			*Period = atof(arg1);
			*SetSpeed = atof(arg2);
			*SetRot = atof(arg3);
			*run = 1;

// STOP [period]
		}else if( !strncmp( com, "STOP", 4) ){
			*Period = atof(arg1);
			*SetSpeed = 0;
			*SetRot = 0;
			*run = 0;

// BRAKE [period]
		}else if( !strncmp( com,  "BRAKE", 5) ){
			*Period = atof(arg1);
			*SetSpeed = 0;
			*SetRot = 0;
			*run = -1;

// END
		}else if( !strncmp( com, "END", 3) ){
			*Period = 0;
			*SetSpeed = 0;
			*SetRot = 0;
			*run = 0;
			ret = -1;
		}

//Quit while loop
		break;
		
	}//end of while(1)	

	return ret;
}


/*
	Dead Zone をキャンセルするために，y = a ln( b x + 1)で値を変換する。
	(x=0 のとき y=0となる。）
	a, b は，この曲線が (x0,y0) を通るなら，
	exp(y0/a) = b x0 + 1 
	からもとめられる。　a もしくは　b　は任意なので，これで曲線の形を決めることができる。

*/
double RemoveDeadZone(double in){
	double out = 0;
	static double a, b;
	static int first = 1;
	if(first){
		double x0, y0;
		x0 = 0.7;
		y0 = 0.7;
		a = 0.6;
		b = (exp(y0/a)-1)/x0;
		first = 0;
	}

	if(in>0)
		out =  a*log( b*in+1);
	if(in<0)
		out = -a*log(-b*in+1);


	return out;
}


int main(int argc, char* argv[]){
	int i, ret;
	char str[256];
	char *com, *period, *arg1, *arg2;
	double speed;
	FILE *fp, *fpl;
	double dt;
	short accI[3], rotI[3], tempI;
	double acc[3], rot[3], temp;
	double SF_Acc, SF_Gyro, SF_Temp;
	double RotZ;
	double OffsetRz;
	struct timeval tm, tm0, tm_next;
	double SpeedR, SpeedL, CtrlRot;
	double RotSet, RotNow, SpeedSet;
	int run, end;
	char fname_log[] = "log.dat";

	DRV8830 MotR, MotL;
	MPU9250 Sensor;
	PID pid;

//Read Command file
	if(argc != 2){
		printf("Usage: %s [command file]\n",argv[0]);
		return -1;
	}
	if(NULL == (fp=fopen(argv[1], "r"))){
		printf("File Open Error: %s\n", argv[1]);
		return -1;
	}

//Log file
	if(NULL == (fpl=fopen(fname_log, "w"))){
		printf("File Open Error: %s\n", fname_log);
		return -1;
	}
		


//Init MPU9250
	ret = Sensor.CheckConnection();
	if(!ret){
		printf("MPU9250 Not Found\n");
		return -1;
	}
	Sensor.Init();
	
	Sensor.CheckConnection_Mag();
	if(!ret){
		printf("MPU9250(Magnetometer) Not Found\n");
		return -1;
	}

	Sensor.SetAccFullScale (Acc_FS_4G);
	Sensor.SetGyroFullScale( Gyro_FS_500dps);
	//Sensor.SetGyroFullScale( Gyro_FS_250dps);
	Sensor.SetAccRate( Acc_BW460Hz_SR1k );
	Sensor.SetGyroRate( Gyro_BW250Hz_SR8k );

	printf("MPU9250 Initialized\n");


	//return 0;

//Init Motor Drivers
	int fdR = MotR.Init( 0x63 );
	int fdL = MotL.Init( 0x64 );

	printf("fd(R)=%d\n", fdR);
	printf("fd(L)=%d\n", fdL);

	if( (fdR<0) || (fdL<0)){
		return -1;
	}

	MotR.PrintFaultMsg( MotR.ReadFault() );
	MotL.PrintFaultMsg( MotL.ReadFault() );

	gettimeofday(&tm0, NULL);
//	tm_next = NextTime(tm0, 2.4);
	tm_next = tm0;

	RotZ = 0;
	pid.SetParam(0.10, 1.1, 0.050);
	pid.SetLimit(0.5, -0.5);


//Measure Bias	
	printf("Now Offset Measuring\n");
	int cnt=0;
	while(1){
		Sensor.ReadData(&tm, accI, rotI, &tempI);
		dt=TimeDiff(tm, tm0);
		
		OffsetRz += rotI[2];
		cnt++;

		if(dt > 1.0) break;
	}

	OffsetRz *= (1.0/(double)cnt) * Sensor.ScaleFactor_Gyro();
	printf("Offset (Rot Z) = %lf [dps]", OffsetRz);


	while(1){
//Read Sensor outputs
		Sensor.ReadData(&tm, accI, rotI, &tempI);
		dt=TimeDiff(tm, tm0);
		for(int i=0; i<3; i++){
			acc[i] = (double)accI[i] * Sensor.ScaleFactor_Acc();
			rot[i] = (double)rotI[i] * Sensor.ScaleFactor_Gyro();
		}
		temp = (double)tempI * Sensor.ScaleFactor_Temp() + Sensor.Offset_Temp();


//Current Heading Direction
		rot[2] -= OffsetRz;
		RotNow += rot[2]*dt;
		printf("%lf, RotZ=%lf\n", dt, RotNow);

//Read Next command
		if(TimeDiff(tm, tm_next) > 0){
			double Period;
			end = ReadNextCommand(fp, &SpeedSet, &RotSet, &Period, &run);

			if(end < 0) break;

			tm_next = NextTime(tm, Period);
			pid.Reset();
		}

//Control value (Rotation)
		//CtrlRot = pid.Exec( RotNow-RotSet, dt);
		CtrlRot = pid.Exec( RotNow-RotSet, rot[2], dt);
		//CtrlRot = RemoveDeadZone(CtrlRot);

		SpeedR = SpeedSet - CtrlRot;
		SpeedL = SpeedSet + CtrlRot;

		if(SpeedR > 1.0) SpeedR = 1.0;
		if(SpeedR < -1.0) SpeedR = -1.0;
		if(SpeedL > 1.0) SpeedL = 1.0;
		if(SpeedL < -1.0) SpeedL = -1.0;

		if(run == 1){	
			MotR.Run( -SpeedR * 5.06 );
			MotL.Run( SpeedL * 5.06 );

		}else if(run == -1){
			MotR.Brake();
			MotL.Brake();


		}else{
			MotR.Stop();
			MotL.Stop();
		}
		
//Write to log file
		fprintf(fpl, "%lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf\n",
			tm.tv_sec+tm.tv_usec*1e-6,	
			acc[0], acc[1], acc[2], rot[0], rot[1], rot[2],	temp,
			RotNow, RotSet, SpeedR, SpeedL);


		tm0 = tm;
	}

	MotR.Stop();
	MotL.Stop();


	fclose(fp);
	fclose(fpl);

	return 0;
}
