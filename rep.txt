#
#RUN [period] [speed(0.4~1.0)] [Rotation]
#STOP [period]
#BREAK [period]
#

RUN 0.5  0.6  0.0
BRAKE 0.2
STOP  0.2
RUN 0.5  -0.6  0.0
BRAKE 0.2
STOP  0.2

RUN 0.5  0.6  0.0
BRAKE 0.2
STOP  0.2
RUN 0.5  -0.6  0.0
BRAKE 0.2
STOP  0.2

RUN 0.5  0.6  0.0
BRAKE 0.2
STOP  0.2
RUN 0.5  -0.6  0.0
BRAKE 0.2
STOP  0.2

RUN 0.5  0.6  0.0
BRAKE 0.2
STOP  0.2
RUN 0.5  -0.6  0.0
BRAKE 0.2
STOP  0.2

STOP  0.5
END
