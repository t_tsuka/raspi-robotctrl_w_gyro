plot "log.dat" u 1:2 w l title "Acc.X", "" u 1:3 w l title "Acc.Y", "" u 1:4 w l title "Acc.Z"
plot "log.dat" u 1:5 w l title "Gyro.X", "" u 1:6 w l title "Gyro.Y", "" u 1:7 w l title "Gyro.Z"
plot "log.dat" u 1:9 w l title "{/Symbol Q}", "" u 1:10 w l title "{/Symbol Q}_{set}"
plot "log.dat" u 1:11 w l title "Motor.R.Setval", "" u 1:12 w l title "Motor.L.Setval"

